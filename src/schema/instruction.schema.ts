import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class Instruction {
  @Prop({ type: Object })
  content: object;
}

export const InstructionSchema = SchemaFactory.createForClass(Instruction);
