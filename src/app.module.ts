import { Module } from '@nestjs/common';
import { AppController } from './controller/app.controller';
import { AppService } from './service/app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule } from 'nestjs-pino';
import { MongooseModule } from '@nestjs/mongoose';
import { Instruction, InstructionSchema } from './schema/instruction.schema';
import { CMSService } from './service/cms.service';
import { CMSController } from './controller/cms.controller';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    LoggerModule.forRoot({
      pinoHttp: {
        formatters: {
          level: (label) => {
            return { level: label.toUpperCase() };
          },
        },
        customLevels: {
          emergerncy: 80,
          alert: 70,
          critical: 60,
          error: 50,
          warn: 40,
          notice: 30,
          info: 20,
          debug: 10,
        },
        useOnlyCustomLevels: true,
        transport: {
          target: 'pino-pretty',
          options: {
            singleLine: true,
            colorize: true,
            levelFirst: true,
            translateTime: 'SYS:standard',
            ignore: 'hostname,pid',
          },
        },
      },
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        uri: `mongodb://${configService.get<string>(
          'MONGO_HOST',
        )}:${configService.get<string>(
          'MONGO_PORT',
        )}/${configService.get<string>('MONGO_DB')}`,
      }),
    }),
    MongooseModule.forFeature([
      { name: Instruction.name, schema: InstructionSchema },
    ]),
    AuthModule,
  ],
  controllers: [AppController, CMSController],
  providers: [AppService, CMSService],
})
export class AppModule {}
