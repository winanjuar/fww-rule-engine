import { IsNotEmpty, IsString } from 'class-validator';

export class FactDestinationDto {
  @IsString()
  @IsNotEmpty()
  readonly id: string;

  @IsString()
  @IsNotEmpty()
  readonly destination: string;
}
