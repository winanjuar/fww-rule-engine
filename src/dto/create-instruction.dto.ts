import { IsNotEmpty, IsObject } from 'class-validator';

export class CreateInstructionDto {
  @IsObject()
  @IsNotEmpty()
  readonly content: object;
}
