import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class FactFastPaymentDto {
  @IsString()
  @IsNotEmpty()
  readonly id: string;

  @IsNumber()
  @IsNotEmpty()
  readonly minutesDurationFromBooking: number;
}
