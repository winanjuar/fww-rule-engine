export enum EInstructionName {
  DESTINATION = 'discount-destination',
  FASTPAYMENT = 'discount-fast-payment',
}
