import { Document } from 'mongoose';

export interface IFullInstruction extends Document {
  readonly content: IBasicInstruction;
}

export interface IBasicInstruction {
  readonly name: string;
  readonly attributes: Array<object>;
  readonly decisions: Array<object>;
}
