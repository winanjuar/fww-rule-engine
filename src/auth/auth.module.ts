import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { AuthBasicStrategy } from './auth-basic.strategy';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [PassportModule, ConfigModule],
  providers: [AuthBasicStrategy],
})
export class AuthModule {}
