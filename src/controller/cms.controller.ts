import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Logger,
  Param,
  Post,
  Res,
} from '@nestjs/common';
import type { Response } from 'express';
import { CreateInstructionDto } from 'src/dto/create-instruction.dto';
import { CMSService } from 'src/service/cms.service';

@Controller({ version: '1', path: 'cms' })
export class CMSController {
  private readonly logger = new Logger(CMSController.name);
  constructor(private readonly cmsService: CMSService) {}
  @Post()
  async create(
    @Res() response: Response,
    @Body() instruction: CreateInstructionDto,
  ) {
    this.logger.log('[POST] /api/v1/cms');
    try {
      const newInstruction = await this.cmsService.create(instruction);

      this.logger.log('Return new instruction');
      return response.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Instruction has been saved successfully',
        data: newInstruction,
      });
    } catch (err) {
      return response.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
        statusCode: HttpStatus.UNPROCESSABLE_ENTITY,
        message: 'Error: Instruction not saved!',
        error: 'Unprocessable Entity',
      });
    }
  }

  @Get()
  async getAll(@Res() response: Response) {
    this.logger.log('[GET] /api/v1/cms');
    try {
      const instructions = await this.cmsService.findAll();
      return response.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Instructions load successfully',
        data: instructions,
      });
    } catch (err) {
      if (err.response.statusCode === 404) {
        return response.status(HttpStatus.NOT_FOUND).json({
          statusCode: HttpStatus.NOT_FOUND,
          message: err.response.message,
          error: 'Not Found',
        });
      } else {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
          message: err.response.message,
          error: 'Internal Server Error',
        });
      }
    }
  }

  @Get(':id')
  async getOne(@Res() response: Response, @Param('id') id: string) {
    this.logger.log(`[GET] /api/v1/cms/${id}`);
    try {
      const instruction = await this.cmsService.findOne(id);
      return response.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Instruction found!',
        data: instruction,
      });
    } catch (err) {
      if (err.response.statusCode === 404) {
        return response.status(HttpStatus.NOT_FOUND).json({
          statusCode: HttpStatus.NOT_FOUND,
          message: err.response.message,
          error: 'Not Found',
        });
      } else {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
          message: err.response.message,
          error: 'Internal Server Error',
        });
      }
    }
  }
}
