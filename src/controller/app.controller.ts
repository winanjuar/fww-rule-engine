import {
  Body,
  Controller,
  HttpStatus,
  Logger,
  Post,
  Res,
  UnprocessableEntityException,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import type { Response } from 'express';
import { readFileSync } from 'fs';
import { Engine, RuleProperties } from 'json-rules-engine';
import { FactDestinationDto } from 'src/dto/fact-destination.dto';
import { FactFastPaymentDto } from 'src/dto/fact-fast-payment.dto';
import { EInstructionName } from 'src/enum/instruction-name.enum';
import { CMSService } from 'src/service/cms.service';

@Controller({ version: '1' })
export class AppController {
  private readonly logger = new Logger(AppController.name);
  constructor(private readonly cmsService: CMSService) {}

  @UseGuards(AuthGuard('basic'))
  @Post('destination')
  async destination(
    @Body() check: FactDestinationDto,
    @Res() response: Response,
  ) {
    this.logger.log('[POST] /api/v1/destination');
    const instruction = await this.cmsService.findOne(check.id);
    if (instruction.content.name !== EInstructionName.DESTINATION) {
      throw new UnprocessableEntityException('Mismatch rules');
    }

    const ruleDestination = instruction.content.decisions[0] as RuleProperties;

    const engine = new Engine();
    engine.addRule(ruleDestination);

    const facts = { destination: check.destination.toUpperCase() };

    engine
      .run(facts)
      .then(({ events }) => {
        events.forEach((event) => this.logger.log(event.params.message));
      })
      .catch((error) => this.logger.error(error));

    engine.on('success', (event) => {
      this.logger.log('Rules discount destination fulfilled');
      response.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Rules discount destination fulfilled',
        data: {
          statusRule: 'Fulfilled',
          discountDestination: event.params.discountDestination,
        },
      });
    });

    engine.on('failure', () => {
      this.logger.log('Rules discount destination not fulfilled');
      response.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Rules discount destination not fulfilled',
        data: {
          statusRule: 'Not Fulfilled',
          discountDestination: 0,
        },
      });
    });
  }

  @UseGuards(AuthGuard('basic'))
  @Post('fast-payment')
  async fastPayment(
    @Body() check: FactFastPaymentDto,
    @Res() response: Response,
  ) {
    this.logger.log('[POST] /api/v1/fast-payment');
    const instruction = await this.cmsService.findOne(check.id);
    if (instruction.content.name !== EInstructionName.FASTPAYMENT) {
      throw new UnprocessableEntityException('Mismatch rules');
    }

    const ruleFastPayment = instruction.content.decisions[0] as RuleProperties;

    const engine = new Engine();
    engine.addRule(ruleFastPayment);

    const facts = {
      minutesDurationFromBooking: check.minutesDurationFromBooking,
    };

    engine
      .run(facts)
      .then(({ events }) => {
        events.forEach((event) => this.logger.log(event.params.message));
      })
      .catch((error) => this.logger.error(error));

    engine.on('success', (event) => {
      this.logger.log('Rules discount fast payment fulfilled');
      response.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Rules discount fast payment fulfilled',
        data: {
          statusRule: 'Fulfilled',
          discountFastPayment: event.params.discountFastPayment,
        },
      });
    });

    engine.on('failure', () => {
      this.logger.log('Rules discount fast payment not fulfilled');
      response.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Rules discount fast payment not fulfilled',
        data: {
          statusRule: 'Not Fulfilled',
          discountFastPayment: 0,
        },
      });
    });
  }

  @UseGuards(AuthGuard('basic'))
  @Post('destination-file')
  async destinationFile(
    @Body() check: FactDestinationDto,
    @Res() response: Response,
  ) {
    this.logger.log('[POST] /api/v1/destination-file');
    const instruction = JSON.parse(
      readFileSync('./asset/discount-destination.json', 'utf8'),
    );

    if (instruction.name !== EInstructionName.DESTINATION) {
      throw new UnprocessableEntityException('Mismatch rules');
    }

    const ruleDestination = instruction.decisions[0] as RuleProperties;

    const engine = new Engine();
    engine.addRule(ruleDestination);

    const facts = { destination: check.destination.toUpperCase() };

    engine
      .run(facts)
      .then(({ events }) => {
        events.forEach((event) => this.logger.log(event.params.message));
      })
      .catch((error) => this.logger.error(error));

    engine.on('success', (event) => {
      this.logger.log('Rules discount destination fulfilled');
      response.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Rules discount destination fulfilled',
        data: {
          statusRule: 'Fulfilled',
          discountDestination: event.params.discountDestination,
        },
      });
    });

    engine.on('failure', () => {
      this.logger.log('Rules discount destination not fulfilled');
      response.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Rules discount destination not fulfilled',
        data: {
          statusRule: 'Not Fulfilled',
          discountDestination: 0,
        },
      });
    });
  }

  @UseGuards(AuthGuard('basic'))
  @Post('fast-payment-file')
  async fastPaymentFile(
    @Body() check: FactFastPaymentDto,
    @Res() response: Response,
  ) {
    this.logger.log('[POST] /api/v1/fast-payment');
    const instruction = JSON.parse(
      readFileSync('./asset/discount-fast-payment.json', 'utf8'),
    );
    if (instruction.name !== EInstructionName.FASTPAYMENT) {
      throw new UnprocessableEntityException('Mismatch rules');
    }

    const ruleFastPayment = instruction.decisions[0] as RuleProperties;

    const engine = new Engine();
    engine.addRule(ruleFastPayment);

    const facts = {
      minutesDurationFromBooking: check.minutesDurationFromBooking,
    };

    engine
      .run(facts)
      .then(({ events }) => {
        events.forEach((event) => this.logger.log(event.params.message));
      })
      .catch((error) => this.logger.error(error));

    engine.on('success', (event) => {
      this.logger.log('Rules discount fast payment fulfilled');
      response.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Rules discount fast payment fulfilled',
        data: {
          statusRule: 'Fulfilled',
          discountFastPayment: event.params.discountFastPayment,
        },
      });
    });

    engine.on('failure', () => {
      this.logger.log('Rules discount fast payment not fulfilled');
      response.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Rules discount fast payment not fulfilled',
        data: {
          statusRule: 'Not Fulfilled',
          discountFastPayment: 0,
        },
      });
    });
  }
}
