import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Instruction } from '../schema/instruction.schema';
import { IFullInstruction } from 'src/interface/full-instruction.interface';
import { CreateInstructionDto } from '../dto/create-instruction.dto';

@Injectable()
export class CMSService {
  private readonly logger = new Logger(CMSService.name);
  constructor(
    @InjectModel(Instruction.name)
    private instructionModel: Model<IFullInstruction>,
  ) {}

  async create(instruction: CreateInstructionDto): Promise<IFullInstruction> {
    const newInstruction = await new this.instructionModel(instruction).save();

    this.logger.log('Process create new instruction');
    return newInstruction;
  }

  async findOne(id: string): Promise<IFullInstruction> {
    const instruction = await this.instructionModel.findById(id).exec();
    if (!instruction) {
      throw new NotFoundException(`Instruction #${id} not found`);
    }
    this.logger.log('Process data instruction');
    return instruction;
  }

  async findAll(): Promise<IFullInstruction[]> {
    const instructions = await this.instructionModel.find().exec();
    if (!instructions || instructions.length == 0) {
      throw new NotFoundException(
        'Instruction not found! Maybe collection still empty',
      );
    }
    this.logger.log('Process data instructions');
    return instructions;
  }
}
