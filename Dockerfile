# BUILDER
FROM node:18-alpine AS builder

RUN adduser -D -g '' apps

WORKDIR /usr/src/app

COPY --chown=apps:apps package*.json ./

COPY --chown=apps:apps . .

RUN npm config set strict-ssl && \
  npm ci --legacy-peer-deps && \
  npm run build
RUN npm ci --legacy-peer-deps --only=production && npm cache clean --force

USER apps

# RUNNER
FROM node:18-alpine AS runner

RUN adduser -D -g '' apps

WORKDIR /app

COPY --chown=apps:apps --from=builder /usr/src/app/asset ./asset
COPY --chown=apps:apps --from=builder /usr/src/app/node_modules ./node_modules
COPY --chown=apps:apps --from=builder /usr/src/app/dist ./dist

ARG env
ENV ENVIRONMENT $env

USER apps
# EXPOSE 3007 3007
CMD [ "node", "dist/main.js" ]
