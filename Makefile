SERVICE_NAME := fww-rule-engine
GIT_COMMIT_ID := $(shell git log --format="%H" -n 1)
IMAGE_ID := $(shell docker images --filter=reference=winanjuar/fww-rule-engine --format "{{.ID}}")
CONTAINER_ID := $(shell docker ps -aqf "name=fww-rule-engine")

rebase:
			git fetch && git pull --rebase

prune:
			docker stop $(CONTAINER_ID)
			docker rm $(CONTAINER_ID)
			docker rmi -f $(IMAGE_ID)

build:
			docker build -t winanjuar/${SERVICE_NAME}:${GIT_COMMIT_ID} -t winanjuar/${SERVICE_NAME}:latest .

push:
			docker push winanjuar/${SERVICE_NAME}:latest
			docker push winanjuar/${SERVICE_NAME}:${GIT_COMMIT_ID}

run:
			docker run --env-file /Users/sugengwin/Developer/fww-env/dev_rule_engine.env --name ${SERVICE_NAME} -p $(cport):$(cport) -d winanjuar/${SERVICE_NAME}:latest

logs:
			docker logs -f ${SERVICE_NAME}